import { useState } from 'react'
import logo from './logo.svg'
import './App.css'
import Card from "./components/Card/Card"

function App() {
  const [count, setCount] = useState(0)
  const array = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

  function getRandomImage() {
    let randomNumber = Math.floor((Math.random() * array.length) + 1) 
    
    return array[randomNumber-1]
  }

  return (
    <div>
      <h1>Chuck facts</h1>
      <div className="container">
      {
        array.map((item, index) => {
          return <Card numberImage={getRandomImage()} />
        })
      }
      
      </div>
    </div>
    
  )
}

export default App
