import React, { useState, useEffect } from "react";
import axios from "axios";

const API_URL = "https://api.chucknorris.io/jokes/random"

function CardTextContent() {
    const [content, setContent] = useState("")

    const fetchData = async () => {
        const results = await axios.get(API_URL)
        setContent(results.data.value)
    } 

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>{content}</div>
    );
};

export default CardTextContent;