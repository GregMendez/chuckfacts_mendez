import React from "react";
import CardTextContent from "../CardTextContent/CardTextContent";
import "./CardCSS.css"

const Card = (props: any) => {

    const imageSource = `../public/assets/${props.numberImage}.jpg`

    return(
        <div className="card">
            <img src={imageSource} alt="" />
            <CardTextContent />
        </div>
    );
};

export default Card;